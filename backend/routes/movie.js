const express= require('express')
const db= require('../db')
const utils= require('../utils')
const router= express.Router()

router.get('/getname', (request, response)=>{
    const query= `select * from movie`
    db.execute(query, (error, result)=>{
        response.send(utils.createResult(error, result))
    })
})

router.post('/addmovie', (request, response)=>{
    const{movie_id, movie_title, movie_release_date, movie_time, director_name} = request.body
    const query= `insert into movie 
    values('${movie_id}', '${movie_title}', '${movie_release_date}', '${movie_time}', '${director_name}')
    `
    db.execute(query, (error, result)=>{
        response.send(utils.createResult(error, result))
    })
})

router.put('/updatemovie', (request, response)=>{
    const{movie_id, movie_release_date, movie_time} = request.body
    const query= `update movie 
    set movie_release_date='${movie_release_date}' and
     movie_time= '${movie_time}' where movie_id='${movie_id}'
    `
    db.execute(query, (error, result)=>{
        response.send(utils.createResult(error, result))
    })
})


router.delete('/deletemovie', (request, response)=>{
    const{movie_id} = request.body
    const query= `delete from movie
    where movie_id='${movie_id} `
    db.execute(query, (error, result)=>{
        response.send(utils.createResult(error, result))
    })
})

module.exports= router