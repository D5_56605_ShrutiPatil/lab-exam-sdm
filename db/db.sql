create table movie
(movie_id integer,
movie_title varchar(50),
movie_release_date date,
movie_time time,
director_name varchar(50)
);

insert into movie values(1, "titanic", "1990-02-01", "5", "shruti");
insert into movie values(2, "titanic-2", "1995-02-01", "8", "ajay");
